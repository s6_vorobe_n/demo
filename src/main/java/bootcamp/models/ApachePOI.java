package bootcamp.models;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.impl.piccolo.io.FileFormatException;

public class ApachePOI {

	public static List<String> readWordDocument(String fileName) {
		List<String> text = new ArrayList<String>();
		try {

			File file = new File(fileName);
			FileInputStream fis = new FileInputStream(file.getAbsolutePath());
			XWPFDocument doc = new XWPFDocument(fis);

			List<XWPFTable> table = doc.getTables();

			for (XWPFTable xwpfTable : table) {
				List<XWPFTableRow> row = xwpfTable.getRows();
				for (XWPFTableRow xwpfTableRow : row) {
					List<XWPFTableCell> cell = xwpfTableRow.getTableCells();
					for (XWPFTableCell xwpfTableCell : cell) {
						if (xwpfTableCell != null) {
							System.out.println(xwpfTableCell.getText());
							text.add(xwpfTableCell.getText());
							List<XWPFTable> itable = xwpfTableCell.getTables();
							if (itable.size() != 0) {
								for (XWPFTable xwpfiTable : itable) {
									List<XWPFTableRow> irow = xwpfiTable.getRows();
									for (XWPFTableRow xwpfiTableRow : irow) {
										List<XWPFTableCell> icell = xwpfiTableRow.getTableCells();
										for (XWPFTableCell xwpfiTableCell : icell) {
											if (xwpfiTableCell != null) {
												text.add(xwpfiTableCell.getText());
												System.out.println(xwpfiTableCell.getText());
											}
										}
									}
								}
							}
						}
					}
				}
			}
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return text;
	}

	
}
