package bootcamp.models;

import javax.persistence.*;

import org.springframework.lang.NonNull;

@Entity
@Table(name = "GradeTable")
public class Grade {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;

	@Column(name = "value")
	private short value;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Student getStudent() {
		return student;
	}
	public void setStudent(Student student) {
		this.student = student;
	}

	@OneToOne
	private Student student;
	
	public Grade (short value) {
		setValue(value);
	}
	public Grade() {
		
	}
	
	public long getId_a() {
		return id;
	}

	public void setId_a(int id_a) {
		this.id = id_a;
	}

	public short getValue() {
		return value;
	}

	public void setValue(short value) {
		if(value>0)
			this.value = value;
		else
			value = 0;
	}

	
}
