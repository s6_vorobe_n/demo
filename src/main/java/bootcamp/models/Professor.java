package bootcamp.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "professorTable")
public class Professor extends User{
	
	//@NotNull
	//@Size(min = 3, max = 30)
	@Column(name = "qualification")
	private String qualification;
	
	//@Column(name = "course_id")
	@OneToOne(cascade = {CascadeType.ALL})
	Course course;
	
	
	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Professor(String name, int age, String email, String password) {
		super(name, age, email, password);
	}
	
	public Professor(String name, String email, Course course, String qualification) {
		super(name,email);
		this.course = course;
		setQualification(qualification);
	}
	
	public Professor(String name, int age, String email, String password,  Course course, String qualification) {
		super(name, age, email, password);
		this.course = course;
		setQualification(qualification);
	}
	
	public Professor(String name) {
		super(name);
	}
	
	
	
	public Professor() {
		
	}
	
	public void createCourse(Course course) {
		if(this.course == null)
			this.course = course;
		else
			System.out.println("Course for this professor already exists!");
	}
	public void uploadCourse() {
		
	}
	public void evaluateStudent(Student student, int value) {
		if(course.getStudents().contains(student)) {
			if(student.getGrade() == null)
				student.setGrade(new Grade((short)value));
			else
				student.getGrade().setValue((short)value);;
		}
		
	}
	public void exportGrades() {
		
	}
	public void sendResults() {
		
	}
	
	
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		if(qualification.length() > 3 && qualification.length() < 60)
			this.qualification = qualification;
		else
			this.qualification = null;
	}

	@Override
	public String toString() {
		return "Professor [qualification=" + qualification + ", course=" + course + ", getCourse()=" + getCourse()
				+ ", getQualification()=" + getQualification() + ", getName()=" + getName() + ", getAge()=" + getAge()
				+ ", getEmail()=" + getEmail() + ", getPassword()=" + getPassword() + ", getMathingPassword()="
				+ getMathingPassword() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}
	
	
	
}
