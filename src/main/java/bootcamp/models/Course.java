package bootcamp.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name = "CourseTable")
public class Course {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;
	//@NotNull
	//@Size(min = 3, max = 140)
	@Column(name = "title")
	private String title;
	//@NotNull
	@OneToOne(mappedBy = "course", cascade = {CascadeType.ALL})
	private Professor professor;
	//@NotNull
	//@Size(min = 3, max = 140)
	@Column(name = "evaluationForm", length=400)
	private String evaluationForm;
	//@NotNull
	//@Max(12)
	//@Min(1)
	@Column(name = "creditPoints")
	private int CP;
	@OneToMany(mappedBy = "course")
	private Set<Student> students;
	//@NotNull
	//@Size(min = 3, max = 140)
	@Column(name = "prerequisites", length=400)
	private String prerequisites;
	//@NotNull
	//@Size(min = 3, max = 300)
	@Column(name = "objective",length=400)
	private String objective;
	//@NotNull
	//@Size(min = 3, max = 30)
	@Column(name = "content",length=400)
	private String content;
	
	public Course() {
		
	}
	
	public Course(String title, String evaluationForm, int CP, String prerequisites, String objective, String content) {
		this.title = title;
		this.evaluationForm = evaluationForm;
		setCP(CP);
		this.prerequisites = prerequisites;
		this.objective = objective;
		this.content = content;
	}
	
	public Course(String title, String evaluationForm, String professor, int CP, String prerequisites, String objective, String content) {
		this.title = title;
		this.evaluationForm = evaluationForm;
		setCP(CP);
		this.prerequisites = prerequisites;
		this.objective = objective;
		this.content = content;
	}
	
	
	public long getId_c() {
		return id;
	}
	public void setId_c(long id_c) {
		this.id = id_c;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	
	public Professor getProfessor() {
		return professor;
	}
	public void setProfessor(Professor professor) {
		this.professor = professor;
	}
	
	public String getEvaluationForm() {
		return evaluationForm;
	}
	public void setEvaluationForm(String evaluationForm) {
		this.evaluationForm = evaluationForm;
	}
	public int getCP() {
		return CP;
	}
	public void setCP(int CP) {
		if(CP < 12 && CP > 0)
			this.CP = CP;
		else
			CP = 0;
	}
	public Set<Student> getStudents() {
		return students;
	}
	public void setStudents(Set<Student> students) {
		this.students = students;
	}
	public String getPrerequisites() {
		return prerequisites;
	}
	public void setPrerequisites(String prerequisites) {
		this.prerequisites = prerequisites;
	}
	public String getObjective() {
		return objective;
	}
	public void setObjective(String objective) {
		this.objective = objective;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "Course [id_c=" + id + ", title=" + title + ", evaluationForm="
				+ evaluationForm + ", CP=" + CP + ", students=" + students + ", prerequisites=" + prerequisites
				+ ", objective=" + objective + ", content=" + content + "]";
	}
	
}
