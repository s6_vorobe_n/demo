package bootcamp.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "studentTable")
public class Student extends User{


	@OneToOne(mappedBy = "student", cascade = {CascadeType.ALL})
	private Grade grade;
	
	@ManyToOne(cascade = {CascadeType.ALL})
	private Course course;
	
	public Student() {
		this.grade = new Grade((short)0);
	}
	public Student(String email, String password) {
		super(email, password);
	}
	public Student(String name, short age, String email, String password, Course course, Grade grade) {
		super(name, age, email, password);
		this.course = course;
		this.grade = grade;
	}

	public void registerToCourse() {
		
	}




	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Grade getGrade() {
		return grade;
	}

	public void setGrade(Grade grade) {
		this.grade = grade;
	}
	
	
	
}