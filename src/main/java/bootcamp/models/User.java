package bootcamp.models;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@MappedSuperclass
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id")
	private long id;
	// @NotNull
	// @NotEmpty
	// @Size(min = 1, max = 50)
	@Column(name = "name")
	private String name;

	// @NotNull
	// @NotEmpty
	// @Max(150)
	// @Min(16)
	@Column(name = "age")
	private int age;

	// @NotNull
	// @NotEmpty
	// @Size(min = 1, max = 200)
	@Column(name = "email")
	private String email;

	// @NotNull
	// @NotEmpty
	// @Size(min = 1, max = 150)
	@Column(name = "password")
	private String password;
	public long getUser_id() {
		return id;
	}
	public void setUser_id(long user_id) {
		this.id = user_id;
	}

	private String mathingPassword;

	public User(String name, String email) {
		this.email = email;
		this.name = name;
	}

	public User(long id) {
		this.id = id;
	}
	

	public User(String name, int age, String email, String password) {
		setName(name);
		setAge(age);
		setEmail(email);
		setPassword(password);

	}

	public User(String name) {
		this.name = name;
	}

	public User() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {

		if (name.matches("[a-zA-Z]+[\\s]*[a-zA-Z]*"))
			this.name = name;
		else
			this.name = null;
	}



	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		if (age > 0)
			this.age = age;
		else
			this.age = 0;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		if (email.matches(
				"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"))
			this.email = email;
		else
			this.email = null;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		if (password.length() > 6)
			this.password = password;
		else
			this.password = null;
	}

	public String getMathingPassword() {
		return mathingPassword;
	}

	public void setMathingPassword(String mathingPassword) {
		this.mathingPassword = mathingPassword;
	}


	@Override
	public String toString() {
		return "User [user_id=" + id + ", name=" + name + ", age=" + age + ", email=" + email + ", password="
				+ password + ", mathingPassword=" + mathingPassword + "]";
	}
	
	

}