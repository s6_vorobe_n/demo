package bootcamp.repo;

import org.springframework.data.repository.CrudRepository;

import bootcamp.models.Course;
import bootcamp.models.Grade;

public interface CourseRepo extends CrudRepository<Course, Long> {

}
