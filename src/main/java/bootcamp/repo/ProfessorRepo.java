package bootcamp.repo;


import java.util.List;


import org.springframework.data.repository.CrudRepository;

import bootcamp.models.Grade;
import bootcamp.models.Professor;

public interface ProfessorRepo  extends CrudRepository<Professor, Long>{
	
	public Professor findByEmailAndPassword(String email, String password);


	Professor findByName(String name);
}
