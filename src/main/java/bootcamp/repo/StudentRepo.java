package bootcamp.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import bootcamp.models.Course;
import bootcamp.models.Grade;
import bootcamp.models.Student;

public interface StudentRepo extends CrudRepository<Student, Long>{
	//TODO define methods for queries
	//public Iterable<Student> findByName(String name);
	public Student findByEmailAndPassword(String email, String password);

	public Student findByName(String string);
	
	public List<Student> findAllByCourseId(long id);

}
