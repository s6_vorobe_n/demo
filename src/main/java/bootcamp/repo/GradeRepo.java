package bootcamp.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import bootcamp.models.Grade;

public interface GradeRepo extends CrudRepository<Grade, Long> {

	public Grade findById(long id);

	
	
	
	
}
