package bootcamp.controller;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashSet;

import javax.annotation.Resource;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import bootcamp.helper.InfoForView;
import bootcamp.helper.ListOfInfoForView;
import bootcamp.models.ApachePOI;
import bootcamp.models.Course;
import bootcamp.models.Grade;
import bootcamp.models.Professor;
import bootcamp.models.Student;
import bootcamp.models.User;
import bootcamp.repo.CourseRepo;
import bootcamp.repo.GradeRepo;
import bootcamp.repo.ProfessorRepo;
import bootcamp.repo.StudentRepo;
import bootcamp.repo.UserRepo;
import bootcamp.upload.StorageFileNotFoundException;
import bootcamp.upload.StorageService;

@Controller
public class UserController extends ApachePOI {

	private final StorageService storageService;

	@Autowired
	public UserController(StorageService storageService) {
		this.storageService = storageService;
	}

	@Autowired
	StudentRepo studRepo;
	@Autowired
	GradeRepo gradeRepo;
	@Autowired
	CourseRepo courseRepo;
	@Autowired
	ProfessorRepo profRepo;

	Course c1 = new Course("Java", "exam", "Arturs", 4, "None", "To introduce with OOP", "OOP basics");
	Course c2 = new Course("Python", "exam", "Janis", 4, "None", "To introduce with OOP", "OOP basics");
	Course c3 = new Course("C++", "exam", "Ebus", 2, "None", "To introduce with OOP", "OOP basics");
	Course c4 = new Course("C#", "exam", "Jebus", 4, "None", "To introduce with OOP", "OOP basics");

	Course c = new Course("Anatomy", "exam", 4, "None", "To introduce with OOP", "OOP basics");

	// test simple controller
	@GetMapping("/testDB") // http://localhost:8080/hello
	public String testDB(Model model) {
		System.out.println("working");

		courseRepo.save(c1);
		courseRepo.save(c2);
		courseRepo.save(c3);
		courseRepo.save(c4);

		courseRepo.save(c);

		Grade gr1 = new Grade((short) 9);
		Grade gr2 = new Grade((short) 3);
		Grade gr3 = new Grade((short) 6);
		Grade gr4 = new Grade((short) 8);

		Grade gr5 = new Grade((short) 8);

		Student st1 = new Student("Janis", (short) 18, "sadf@sdf.ru", "asd3fasfd", c1, gr1);
		Student st2 = new Student("Baiba", (short) 34, "sadf@sdf.lv", "asd001fasfd", c2, gr2);
		Student st3 = new Student("Liga", (short) 20, "sad3f@sdf.com", "asd312fasfd", c3, gr3);
		Student st4 = new Student("Janis", (short) 21, "sa1df@sdf.ua", "as312dfasfd", c4, gr4);

		Student st5 = new Student("Miks", (short) 21, "miks.dambrovskis@gmail.com", "123456789", c, gr5);

		gr1.setStudent(st1);
		gr2.setStudent(st2);
		gr3.setStudent(st3);
		gr4.setStudent(st4);

		gr5.setStudent(st5);

		Professor pr1 = new Professor("John Smith", "sadf@sdf.ru", c1, "mg. sc");
		Professor pr2 = new Professor("Sofia Lorenc", "sadf@sdf.ru", c2, "mg. sc");
		Professor pr3 = new Professor("Artur Smith", "sadf@sdf.ru", c3, "mg. sc");
		Professor pr4 = new Professor("Edgars Berzins", "sadf@sdf.ru", c4, "mg. sc");

		Professor pr = new Professor("nikita", 22, "nikita@gmail.com", "123456789", c, "mg. sc");

		c1.setProfessor(pr1);
		c2.setProfessor(pr2);
		c3.setProfessor(pr3);
		c4.setProfessor(pr4);

		c.setProfessor(pr);

		Set<Student> studentList1 = new HashSet<Student>();
		studentList1.add(st1);
		studentList1.add(st2);

		Set<Student> studentList2 = new HashSet<Student>();
		studentList2.add(st3);
		studentList2.add(st4);

		c1.setStudents(studentList1);
		c2.setStudents(studentList1);

		c3.setStudents(studentList2);
		c4.setStudents(studentList2);

		c.setStudents(studentList2);

		studRepo.save(st1);
		studRepo.save(st2);
		studRepo.save(st3);
		studRepo.save(st4);

		studRepo.save(st5);

		profRepo.save(pr1);
		profRepo.save(pr2);
		profRepo.save(pr3);
		profRepo.save(pr4);

		profRepo.save(pr);

		Iterable<Student> studentsFromDB = studRepo.findAll();
		Iterable<Professor> professorsFromDB = profRepo.findAll();
		Iterable<Course> CoursesFromDB = courseRepo.findAll();

		System.out.println("");
		for (Student student : studentsFromDB)
			System.out.println(student.getUser_id() + " " + student.getName() + " " + student.getAge() + " "
					+ student.getEmail() + " " + student.getGrade().getValue() + " " + student.getPassword() + " "
					+ student.getCourse().getTitle() + " Course id: " + student.getCourse().getId_c());

		System.out.println("");
		for (Professor prof : professorsFromDB)
			System.out.println(prof.getName() + " " + prof.getEmail() + " " + prof.getQualification() + " "
					+ prof.getCourse().getTitle() + " Course id: " + prof.getCourse().getId_c());

		System.out.println("");
		for (Course course : CoursesFromDB)
			System.out.println(
					course.getProfessor().getName() + " " + course.getTitle() + " " + course.getStudents().toString());

		model.addAttribute("allStudents", studentsFromDB);
		model.addAttribute("allProfessors", professorsFromDB);
		model.addAttribute("allCourses", CoursesFromDB);
		return "test";
	}

	@GetMapping("/") // localhost:8080/MainMenu
	public String mainMenu() {
		return "mainmenu";
	}

	@GetMapping("/menuP")
	public String ProfMenu() {

		logger.info("User is in Professor Menu");
		return "menuP";

	}

	@GetMapping("/menuS")
	public String StudMenu() {

		logger.info("User is in Student Menu");
		return "menuS";

	}

	@GetMapping("/registerP") // localhost:8080/registerP
	public String registerProf(Professor professor) {

		logger.info("Professor Registration");
		return "registerprofessor";

	}

	@PostMapping("/registerP")
	public String registerPpost(Professor professor) {

		if (professor.getPassword().equals(professor.getMathingPassword())) {
			profRepo.save(professor);
			logger.info("Professor" + " | " + professor.getName() + " | " + professor.getEmail() + " | "
					+ "Has sucsessfully registered in the system");

			return "redirect:/logInProfessor";
		} else
			return "redirect:/ERRORpass";
	}

	@GetMapping("/ERRORpass")
	public String cat() {
		return "ERRORpass";
	}

	@GetMapping("/registerS") // localhost:8080/registerS
	public String registration(Student student) {
		logger.info("Student Registration");
		return "registerstudent";
	}

	@PostMapping("/registerS")
	public String registerStudent(Student student) {

		if (student.getPassword().equals(student.getMathingPassword())) {
			studRepo.save(student);
			logger.info("Student" + " | " + student.getName() + " " + student.getEmail() + " | "
					+ "Has sucsessfully registered in the system");

			return "redirect:/logInStudent";
		} else
			return "redirect:/ERRORpass";
	}

	@GetMapping("/testregprof")
	public String testReg(Model model) {
		model.addAttribute("newProfessor", profRepo.findAll());
		return "testreg";
	}

	@GetMapping("/testregstudent")
	public String testRegStud(Model model) {
		model.addAttribute("newStudent", studRepo.findAll());
		return "testreg2";
	}

	@GetMapping("/logInStudent") // localhost:8080/logInStudent
	public String showLoginFormForStudent(Student student) {
		logger.info("Student is attempting a log in");
		return "logInStudent";

	}

	@PostMapping("/logInStudent")
	public String showLoginFormForStudentPost(Student student) {

		Student studentFromDB = studRepo.findByEmailAndPassword(student.getEmail(), student.getPassword());
		if (studentFromDB == null) {
			System.out.println("null");
			return "redirect:/logInStudent";

		} else {
			String nameStud = studentFromDB.getName();
			logger.info("Student" + " | " + nameStud + " | " + student.getEmail() + " | "
					+ "Has sucsessfully logged in the system");
			System.out.println(studentFromDB.getUser_id());
			return "redirect:/studentView/" + studentFromDB.getUser_id();

		}
	}

	@RequestMapping(value = "/logInProfessor", method = RequestMethod.GET) // localhost:8080/logInProfessor
	public String showLoginFormForProfessor(Professor professor) {
		logger.info("Professor is attempting a log in");
		return "logInProfessor";

	}

	@PostMapping("/logInProfessor")
	public String showLoginFormForProfessorPost(Professor professor) {
		Professor professorFromDB = profRepo.findByEmailAndPassword(professor.getEmail(), professor.getPassword());
		if (professorFromDB == null)
			return "redirect:/logInProfessor";

		else {
			String nameProf = professorFromDB.getName();
			logger.info("Professor" + " | " + nameProf + " | " + professor.getEmail() + " | "
					+ "Has sucsessfully logged in the system");
			return "redirect:/professorControlPanel/" + professorFromDB.getUser_id();

		}
	}

	@GetMapping("/professorControlPanel/{id}") // localhost:8080/professorControlPanel
	public String professorControlPanelView(@PathVariable(name = "id", required = true) long id, Model model) {

		Optional<Professor> pr = profRepo.findById(id);
		if(pr.get().getCourse() == null) {
			model.addAttribute("name", pr.get().getName());
			model.addAttribute("id", pr.get().getUser_id());
			return "professorcontrolpanel";
		}else {
			model.addAttribute("name", pr.get().getName());
			model.addAttribute("id", pr.get().getUser_id());
			model.addAttribute("title", pr.get().getCourse().getTitle());
			return "professorcontrolpanel";
		}
	}

	@GetMapping("/professorCoursesView/{id}") // localhost:8080/professorCourseView
	public String createCourse(@PathVariable(name = "id", required = true) long id, Model model, Course course) {
		model.addAttribute("id", id);
		return "professorcoursesview";
	}

	@PostMapping("/professorCoursesView/{id}") // localhost:8080/professorCourseView
	public String createCoursePost(@PathVariable(name = "id", required = true) long id, Model model, Course course) {

		Optional<Professor> prof = profRepo.findById(id);
		//course.setProfessor(prof.get());

		prof.get().setCourse(course);

		courseRepo.save(course);

		profRepo.save(prof.get());

		System.out.println(courseRepo.findById(course.getId_c()).get().getId_c());

		logger.info("Professor" + " | " + prof.get().getName() + " | " + prof.get().getEmail() + " | "
				+ "Created a Course");

		return "redirect:/professorControlPanel/"+id;

	}

	@GetMapping("/professorUpload/{id}")
	public String uploadCourse(@PathVariable(name = "id", required = true) long id, Model model) throws IOException {
		model.addAttribute("files",
				storageService.loadAll()
						.map(path -> MvcUriComponentsBuilder
								.fromMethodName(UserController.class, "serveFile", path.getFileName().toString())
								.build().toString())
						.collect(Collectors.toList()));
		return "professorUpload";
	}

	@GetMapping("/files/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Resource> serveFile(@PathVariable String filename) {
		Resource file = (Resource) storageService.loadAsResource(filename);
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.name() + "\"").body(file);
	}

	@PostMapping("/professorUpload/{id}")
	public String uploadCourse1(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes,
			Professor professor, @PathVariable(name = "id", required = true) long id) {
		storageService.store(file);
		;
		redirectAttributes.addFlashAttribute("message",
				"You successfully uploaded " + file.getOriginalFilename() + "!");
		List<String> apacheData = ApachePOI.readWordDocument(file.getOriginalFilename());
		Course dynamic = new Course(apacheData.get(1), apacheData.get(7),
				(Integer.parseInt(apacheData.get(9).charAt(0) + "")), apacheData.get(11), apacheData.get(13),
				apacheData.get(17));

		Professor prrrr = profRepo.findById(id).get();
		prrrr.setCourse(dynamic);
		dynamic.setProfessor(prrrr);

		profRepo.save(prrrr);
		System.out.println(id);
		System.out.println(apacheData.get(17).length());

		logger.info("Professor" + " | " + prrrr.getName() + " | " + prrrr.getEmail() + " | " + "Uploaded a file");

		return "redirect:/apache/" + id;

	}

	@ExceptionHandler(StorageFileNotFoundException.class)
	public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
		return ResponseEntity.notFound().build();
	}

	@GetMapping("/grades/{id}") // localhost:8080/professorGradesView
	public String professorGradesToView(@PathVariable(name = "id", required = true) long id, Model model) {

		// Get course
		Optional<Professor> prof = profRepo.findById(id);
		Course course = prof.get().getCourse();

		// create a list object where all students name, surname and grade will be
		// stored
		ListOfInfoForView dataList = new ListOfInfoForView();

		// get all students in course
		Set<Student> students = course.getStudents();
		

		for (Student s : students) {

			// create new InfoForView object where student name, surname and the grades are
			// stored
			InfoForView ifv = new InfoForView(s.getName(), s.getGrade().getValue());

			// add infoForView object in list
			dataList.addNewItem(ifv);
		}
		
		model.addAttribute("name", prof.get().getName());
		model.addAttribute("dataToEvaluate", dataList);
		model.addAttribute("courseName", course.getTitle());
		return "grades";
	}

	@PostMapping("/grades/{id}")
	public String professorGradesToViewPost(ListOfInfoForView listOfInfoForView, @PathVariable(name = "id", required = true) long id) {
		
		// Get course
		Optional<Professor> prof = profRepo.findById(id);
		Course course = prof.get().getCourse();
		
		Set<Student> students = course.getStudents();
		
		if(students != null) {
			ArrayList<Grade> grades = new ArrayList<Grade>();
			
			for(Student s: students) {
					grades.add(s.getGrade());
			}
			//System.out.println("Grades list: " + grades.get(0).getValue());
			System.out.println(listOfInfoForView);
			
			// print out grades
			for (InfoForView info : listOfInfoForView.listOfInfoForView)
				System.out.println(info.getGrade());
			
			for (int j = 0; j < grades.size(); j++) {
				grades.get(j).setValue(listOfInfoForView.getListOfInfoForView().get(j).getGrade());
				
			
				gradeRepo.save(grades.get(j));
			}
		
		} else {
			System.out.println("There are no students");
		}
		
		return "redirect:/professorControlPanel/" + id;
	}

	@GetMapping("/studentView/{id}") // localhost:8080/studentView
	public String studentToView(@PathVariable(name = "id", required = true) long id, Model model, Course course) {
		Iterable<Course> CoursesFromDB = courseRepo.findAll();
		//course.getTitle();		
		model.addAttribute("AssignCourse", CoursesFromDB);
		return "studentview";
	}
	
	@PostMapping("/studentView/{id}")
	public String studentToViewPost(@PathVariable(name = "id", required = true)long id, Course course) {
		System.out.println(course.getTitle());
		Student studentDB = studRepo.findById(id).get();
		System.out.println(course);
		studentDB.setCourse(course);
		studRepo.save(studentDB);
		return"studentview";
	}

	@GetMapping("/showAllCoursesForStudent") // localhost:8080/showAllCoursesForStudent
	public String showAllCourses(Model model) {
		Iterable<Course> CoursesFromDB = courseRepo.findAll();
		model.addAttribute("allCourses", CoursesFromDB);
		return "showallcoursesforstudent";
	}

	@GetMapping("/apache/{id}")
	public String apacheView(@PathVariable(name = "id", required = true) long id, Model model) {
		List<String> text = new ArrayList<String>();
		text = readWordDocument("Course_JAVA.docx");
		model.addAttribute("Texts", text);
		return "apache";
	}

	// deleting a course
	@GetMapping("/apacheDel/{id}")
	public String apacheViewDel(@PathVariable(name = "id", required = true) long id, Model model) {
		long courseID = profRepo.findById(id).get().getCourse().getId_c();
		Professor pro = profRepo.findById(id).get();
		
		pro.setCourse(null);
		courseRepo.deleteById(courseID);
		profRepo.save(pro);
		
		return "apacheDel";
	}
	@PostMapping("/apacheDel/{id}")
	public String apacheViewDelPost(@PathVariable(name="id", required=true)long id, Model model) {
		
		return "redirect:/professorControlPanel/"+id;
	}
	
	@GetMapping("/apacheDel1/{id}")
	public String apacheViewDel1(@PathVariable(name = "id", required = true) long id, Model model) {
		long courseID = profRepo.findById(id).get().getCourse().getId_c();
		Professor pro = profRepo.findById(id).get();
		pro.setCourse(null);
		courseRepo.deleteById(courseID);
		profRepo.save(pro);
		
		return "apacheDel1";
	}
	@PostMapping("/apacheDel1/{id}")
	public String apacheViewDelPost1(@PathVariable(name="id", required=true)long id, Model model) {
		
		return "redirect:/professorControlPanel/"+id;
	}


	@Autowired
	private JavaMailSender sender;

	@RequestMapping("/sendemail/{id}")
	@ResponseBody
	String home(@PathVariable(name = "id", required = true) long id) {
		try {
			Set<Student> students = profRepo.findById(id).get().getCourse().getStudents();
			System.out.println(students.toString());
			for(Student s : students) {
				System.out.println(s.getEmail()+" " + s.getGrade().getValue());
				sendEmail(s);
			}
			return "Email Sent!";
		} catch (Exception ex) {
			return "Error in sending email: " + ex;
		}

	}


	private void sendEmail(Student student) throws Exception {
		MimeMessage message = sender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);

		helper.setTo(student.getEmail());
		helper.setText("Grade News" + " " + student.getCourse().getTitle() + " " + student.getGrade().getValue() );
		helper.setSubject("New grade has been added");
		sender.send(message);
		logger.info("Professor" + " | " + "Sent an Email to A Student");
	}

	@GetMapping("/apacheTest")
	public String apacheTesting(Professor professor) {
		Workbook wb = new HSSFWorkbook();

		Sheet sheet = wb.createSheet("sheet1");

		Row nameRow = sheet.createRow(0);
		Cell nameCell = nameRow.createCell(0);
		nameCell.setCellValue("Student Name");

		Cell ageCell = nameRow.createCell(1);
		ageCell.setCellValue("Age");

		Cell emailCell = nameRow.createCell(2);
		emailCell.setCellValue("E-mail");

		Cell passCell = nameRow.createCell(3);
		passCell.setCellValue("Password");

		Cell courseCell = nameRow.createCell(4);
		courseCell.setCellValue("Course");

		Cell gradeCell = nameRow.createCell(5);
		gradeCell.setCellValue("Grade");

		Course math = new Course("Math", "Exam", 5, "asd", "learn math", "2+2=4");
		Course eng = new Course("English", "Lesson", 10, "something", "learn past simple", "past simple.");

		Grade grd9 = new Grade((short) 9);
		Grade grd5 = new Grade((short) 5);

		Student stud1 = new Student("Jack", (short) 16, "jack@gmail.com", "password", math, grd9);
		Student stud2 = new Student("Marie", (short) 20, "marie@gmail.com", "anotherpass", eng, grd5);

		ArrayList<Student> studentList = new ArrayList<>();

		studentList.add(stud1);
		studentList.add(stud2);

		int datarow = 1;

		for (Student folks : studentList) {
			Row nameRow1 = sheet.createRow(datarow);
			Cell nameCell1 = nameRow1.createCell(0);
			nameCell1.setCellValue(folks.getName());

			Cell ageCell1 = nameRow1.createCell(1);
			ageCell1.setCellValue(folks.getAge());

			Cell emailCell1 = nameRow1.createCell(2);
			emailCell1.setCellValue(folks.getEmail());

			Cell passCell1 = nameRow1.createCell(3);
			passCell1.setCellValue(folks.getPassword());

			Cell courseCell1 = nameRow1.createCell(4);
			courseCell1.setCellValue(folks.getCourse().getTitle());

			Cell gradeCell1 = nameRow1.createCell(5);
			gradeCell1.setCellValue(folks.getGrade().getValue());

			datarow++;
		}

		try {
			FileOutputStream apache = new FileOutputStream("apacheExcel.xls");
			try {
				wb.write(apache);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				apache.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		logger.info("Professor" + " | " + "Exported EXCEL File");

		return "mainmenu";
	}

	private Logger logger = Logger.getLogger(UserController.class);

}
