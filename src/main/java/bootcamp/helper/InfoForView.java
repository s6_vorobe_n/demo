package bootcamp.helper;

public class InfoForView {
	private String studentName;
	private short grade;

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	
	public short getGrade() {
		return grade;
	}

	public void setGrade(short grade) {
		this.grade = grade;
	}

	public InfoForView(String studentName, short grade) {
		super();
		this.studentName = studentName;
		this.grade = grade;
	}

	public InfoForView() {
	}

}
