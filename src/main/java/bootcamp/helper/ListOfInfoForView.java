package bootcamp.helper;

import java.util.ArrayList;
import java.util.List;

public class ListOfInfoForView {
	public List<InfoForView> listOfInfoForView;

	
	public ListOfInfoForView(List<InfoForView> listOfInfoForView) {
		super();
		this.listOfInfoForView = listOfInfoForView;
	}
	
	public List<InfoForView> getListOfInfoForView() {
		return listOfInfoForView;
	}

	public void setListOfInfoForView(List<InfoForView> listOfInfoForView) {
		this.listOfInfoForView = listOfInfoForView;
	}

	public ListOfInfoForView() {
		super();
		this.listOfInfoForView = new ArrayList<>();
	}
	
	public void addNewItem(InfoForView data)
	{
		this.listOfInfoForView.add(data);
	}

	@Override
	public String toString() {
		return "ListOfInfoForView [allDataForGradeEvaluateView=" + listOfInfoForView + "]";
	}
	
	

}
