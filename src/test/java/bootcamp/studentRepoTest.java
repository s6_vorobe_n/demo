package bootcamp;

import bootcamp.repo.StudentRepo;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import bootcamp.models.Course;
import bootcamp.models.Grade;
import bootcamp.models.Professor;
import bootcamp.models.Student;

@RunWith(SpringRunner.class)

@SpringBootTest
public class studentRepoTest {

	@Autowired
	StudentRepo studRepo2;
	
	@Test
	public void testStudentInsert()
	{
		Course c1 = new Course("Java", "exam", 4, "None", "To introduce with OOP", "OOP basics");
		
		Student st1 = new Student("Janis", (short)18, "sadf@sdf.ru", "asd3fasfd", c1, new Grade((short)9));
	
		studRepo2.save(st1);
		
		Student pFromDB = studRepo2.findByName("Janis");
		
		assertEquals(st1.getName(), pFromDB.getName());
		
		
	}
	
	@Test
	public void testStudentDelete()
	{
		Course c1 = new Course("Java", "exam", 4, "None", "To introduce with OOP", "OOP basics");
		
		Student st1 = new Student("Janiss", (short)18, "sadf@sdf.ru", "asd3fasfd", c1, new Grade((short)9));
	
		studRepo2.save(st1);
		studRepo2.delete(st1);
		
		Student pFromDB = studRepo2.findByName("Janiss");
		
		assertEquals(null, pFromDB);
		
		
	}
	
}
