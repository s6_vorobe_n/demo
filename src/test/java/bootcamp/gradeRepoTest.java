package bootcamp;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import bootcamp.models.Course;
import bootcamp.models.Grade;
import bootcamp.models.Professor;
import bootcamp.repo.GradeRepo;


@RunWith(SpringRunner.class)

@SpringBootTest
public class gradeRepoTest {
	@Autowired
	GradeRepo GradRepo2;
	
	@Test
	public void testGradeInsert() {
		
		Grade gr1 = new Grade((short)9);
		
		GradRepo2.save(gr1);
		
		Grade gradeFromDb = GradRepo2.findById(gr1.getId_a());

		
		assertEquals(gr1.getValue(), gradeFromDb.getValue());
		
		
	}
	
	@Test
	public void testGradeDelete()
	{
		Grade gr1 = new Grade((short)9);
	
		GradRepo2.save(gr1);
		GradRepo2.delete(gr1);
		
		Grade gradeFromDb = GradRepo2.findById(gr1.getId_a());
			
		assertEquals(null, gradeFromDb);
		
	}	
}
