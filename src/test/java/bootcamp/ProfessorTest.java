package bootcamp;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import bootcamp.models.Course;
import bootcamp.models.Professor;

@SpringBootTest
public class ProfessorTest {

	Course c1 = new Course("Java", "exam", 4, "None", "To introduce with OOP", "OOP basics");
	Course c2 = new Course("Python", "exam", 4, "None", "To introduce with OOP", "OOP basics");
	Course c3 = new Course("C++", "exam", 2, "None", "To introduce with OOP", "OOP basics");
	Course c4 = new Course("C#", "exam", 4, "None", "To introduce with OOP", "OOP basics");

	Professor pr1 = new Professor("John Smith", 49, "sadf@sdf.ru", "qwerty123456", c1, "mg");
	Professor pr2 = new Professor("Sofia Lorenc 123", -28, "sadf@sdf.lv", "qwert", c2, "mg. sc");
	Professor pr3 = new Professor("Artur Smith fffff", 31, "sadfsdf.ru", "qwerty123456", c3, "mg. sc. bhbhb bhbhbhb");
	Professor pr4 = new Professor("Edgars Berzins 12312", 38, "sadf@sdf.", "qwerty123456", c4,
			"mg. sc.......................................................................");

	@Test
	public void testForNegativeAge() {
		assertEquals(49, pr1.getAge());
		assertEquals(0, pr2.getAge());
	}

	@Test
	public void testForOnlyLettersName() {
		assertEquals("John Smith", pr1.getName());
		assertEquals(null, pr2.getName());
		assertEquals(null, pr3.getName());
		assertEquals(null, pr4.getName());
	}

	@Test
	public void testQualification() {
		assertEquals(null, pr1.getQualification());
		assertEquals("mg. sc", pr2.getQualification());
		assertEquals("mg. sc. bhbhb bhbhbhb", pr3.getQualification());
		assertEquals(null, pr4.getQualification());
	}

	@Test
	public void testForPasswordLength() {
		assertEquals("qwerty123456", pr1.getPassword());
		assertEquals(null, pr2.getPassword());
	}

	@Test
	public void testForEmail() {
		assertEquals("sadf@sdf.ru", pr1.getEmail());
		assertEquals("sadf@sdf.lv", pr2.getEmail());
		assertEquals(null, pr3.getEmail());
		assertEquals(null, pr4.getEmail());
	}

}
