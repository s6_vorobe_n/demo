package bootcamp;


import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import bootcamp.models.Course;
import bootcamp.models.Grade;
import bootcamp.models.Student;

@SpringBootTest
public class studentTest {
	
	Student st11 = new Student("Janis", (short)18, "sadf@sdf.ru", "asd3fasfd", new Course(), new Grade((short)9));
	Student st12 = new Student("Janis12", (short)-18, "sadfsdf.com", "asd3", new Course(), new Grade((short)-7));
	
	@Test
	public void testForOnlyLettersName() {
		assertEquals("Janis",st11.getName());
		assertEquals(null,st12.getName());
	}
	@Test
	public void testForNegativeAge() {
		
		assertEquals(18,st11.getAge());
		assertEquals(0,st12.getAge());
	}
	
	@Test
	public void testForEmail() {
		assertEquals("sadf@sdf.ru",st11.getEmail());
		assertEquals(null,st12.getEmail());
	}

	@Test
	public void testForPasswordLength() {
		assertEquals("asd3fasfd",st11.getPassword());
		assertEquals(null,st12.getPassword());
	}
	
	@Test
	public void testForNegativeGrade() {
		assertEquals(9,st11.getGrade().getValue());
		assertEquals(0,st12.getGrade().getValue());
	}
	
}
