package bootcamp;

import bootcamp.repo.ProfessorRepo;
import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;

import bootcamp.models.Course;
import bootcamp.models.Professor;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PracticeDemoApplication.class)
public class ProfessorRepoTest {

	@Autowired
	ProfessorRepo profRepo2;
	
	@Test
	public void testProfessorInsert()
	{
		Course c1 = new Course("Java", "exam", 4, "None", "To introduce with OOP", "OOP basics");
		
		Professor pr1 = new Professor("John S", 49, "sadf@sdf.ru", "qwerty123456", c1, "mg. sc.");
	
		profRepo2.save(pr1);
		System.out.println(pr1.getName());
		
		Professor pFromDB = profRepo2.findByName("John S");
		
		assertEquals(pr1.getName(), pFromDB.getName());
	}
	
	@Test
	public void testProfessorDelete()
	{
		Course c1 = new Course("Java", "exam", 4, "None", "To introduce with OOP", "OOP basics");
		
		Professor pr1 = new Professor("John SS", 49, "sadf@sdf.ru", "qwerty123456", c1, "mg. sc.");
	
		profRepo2.save(pr1);
		profRepo2.delete(pr1);
		
		Professor pFromDB = profRepo2.findByName("John SS");
			
		assertEquals(null, pFromDB);
		
	}	
}
