package bootcamp;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import bootcamp.models.Course;

@SpringBootTest
public class courseTest {
	
	Course c1 = new Course("Java", "exam", 4,"None","To introduce with OOP", "OOP basics");
	Course c2 = new Course("Python","exam", 18,"None","To introduce with OOP", "OOP basics");
	Course c3 = new Course("C++","exam", -2,"None","To introduce with OOP", "OOP basics");
	Course c4 = new Course("C#","exam", 0,"None","To introduce with OOP", "OOP basics");
	
	@Test
	public void testForCorrectCP() {
		
		assertEquals(4,c1.getCP());
		assertEquals(0,c2.getCP());
		assertEquals(0,c3.getCP());
		assertEquals(0,c4.getCP());
	}
}
